from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.book_list_wrapper, name='book-list-wrapper'),
    path('list_data/', views.BookListView.as_view(), name='book-list'),
    path('create_random_book/', views.create_random_book_request, name='create-random-book')
]
