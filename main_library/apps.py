from django.apps import AppConfig


class MainLibraryConfig(AppConfig):
    name = 'main_library'
