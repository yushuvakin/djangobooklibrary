import datetime
import random
import string

from BookLibrary.celery import app
from .models import Book


@app.task
def create_random_book():
    try:
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        description = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        pages_count = random.randint(1, 100)
        publication_date = datetime.datetime.now()
        b = Book(name=name, description=description, pages_count=pages_count, publication_date=publication_date)
        b.save()
    except Exception as exc:
        # overrides the default delay to retry after 1 minute
        print("Book creation exception", str(exc))


@app.task
def create_random_book_ret():
    try:
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        description = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        pages_count = random.randint(1, 100)
        publication_date = datetime.datetime.now()
        b = Book(name=name, description=description, pages_count=pages_count, publication_date=publication_date)
        b.save()
        return b.id
    except Exception as exc:
        # overrides the default delay to retry after 1 minute
        print("Book creation exception", str(exc))
