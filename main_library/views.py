from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.views.generic.list import ListView

from .models import Book
from .tasks import create_random_book
from BookLibrary.settings import CELERY_BROKER_URL


def index(request):
    return render(request, "main_library/index.html", context={'num_books': Book.objects.all().count(), 'brokerurl': CELERY_BROKER_URL})


def book_list_wrapper(request):
    return render(request, "main_library/book_list_wrapper.html", context={'num_books': Book.objects.all().count()})


def create_random_book_request(request):
    create_random_book.delay()
    return HttpResponse("Book id = ssss")


class BookListView(ListView):
    model = Book
    template_name = 'main_library/book_list.html'
    context_object_name = 'books'
