amqp==2.2.2
awsebcli==3.12.3
billiard==3.5.0.3
boto3==1.6.2
botocore==1.9.2
celery==4.1.0
cement==2.8.2
colorama==0.3.7
Django==2.0.2
django-celery-beat==1.1.1
django-celery-results==1.0.1
dockerpty==0.4.1
docopt==0.6.2
docutils==0.14
jmespath==0.9.3
kombu==4.1.0
pathspec==0.5.5
psycopg2-binary==2.7.4
python-dateutil==2.6.1
pytz==2018.3
PyYAML==3.12
requests==2.9.1
s3transfer==0.1.13
semantic-version==2.5.0
six==1.11.0
tabulate==0.7.5
termcolor==1.1.0
vine==1.1.4
websocket-client==0.47.0
